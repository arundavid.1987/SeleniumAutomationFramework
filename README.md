# Selenium Automation Framework




## Project Overview

This project is a Java-based test automation framework using Selenium WebDriver and Page Object Pattern. It aims to provide an organized and maintainable structure for writing UI tests for Web Applicationshttps://gitlab.com/arundavid.1987/SeleniumAutomationFramework.git.

## Table of Contents

- [Getting Started](#getting-started)
- [Project Structure](#project-structure)
- [How to Run Tests](#how-to-run-tests)
- [Command & Customization](#customization)
- [Features](#contributing)
- [License](#license)

## Getting Started

To get started with this project, follow these steps:

1. Install Java 8 & Above & Maven
2. Clone the below Repo:

'git clone https://gitlab.com/arundavid.1987/SeleniumAutomationFramework.git'

## Project Structure

- src/test/java/
    - com.example.tests/
        - BaseTest.java (Contains setup and teardown methods for tests)
        - pages/ (Contains page objects)
            - HomePage.java
            - LoginPage.java
            - ... (Other page objects)
        - tests/ (Contains test classes)
            - LoginTests.java
            - MenuVerificationTests.java
            - ... (Other test classes)
    - com.example.utils/
        - DriverManager.java (Manages the WebDriver instance)
        - ConfigReader.java (Reads test configuration)
        - ... (Other utility classes)
- src/test/resources/
    - messages_en.properties 
    - messages_fr.properties

3. Command & Customizations

4. Features 

		Parrallel runs 
		Skip on Failure
		Localization 
		Pipeline setup
