package com.demo.autoInsurance.Testcases;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.demo.autoInsurance.pages.HomePage;



public class MenuVerificationTest {
    private WebDriver driver;
    private HomePage homePage;

    @BeforeMethod
    public void setUp() {
        
        driver = new ChromeDriver();  //
        driver.get("http://sampleapp.tricentis.com/101/"); 
        driver.manage().window().maximize();
        homePage = new HomePage(driver);
    }

    @Test
    public void testMenuItems() {
        // Click on menu item 1
    	Assert.assertTrue(homePage.isMenuDisplayed("Automobile"));
    	Assert.assertTrue(homePage.isMenuDisplayed("Truck"));
    	Assert.assertTrue(homePage.isMenuDisplayed("Motorcycle"));
    }
    
    @Test
    public void testMenuSelection() {
        // Click on menu item 1
    	homePage.selectMenu("Automobile");
    	homePage.selectMenu("Truck");
    	homePage.selectMenu("Motorcycle");
    }

    @AfterMethod
    public void tearDown() {
        // Close the browser after each test
        driver.quit();
    }
}