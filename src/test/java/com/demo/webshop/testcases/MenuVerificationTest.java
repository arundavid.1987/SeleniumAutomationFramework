package com.demo.webshop.testcases;

import java.util.Locale;
import java.util.ResourceBundle;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.demo.webshop.pages.HomePage;
import com.demo.webshop.pages.LoginPage;
import com.utils.ExtentListeners;
import com.utils.setupdriver.SetupTestDriver;

public class MenuVerificationTest {
	private WebDriver driver;
	private HomePage homePage;
	Locale locale;
	ResourceBundle messages;

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "browser", "url", "lang" })
	public void setUp(String browser, String url, String lang) throws Exception {

		SetupTestDriver setupTestDriver = new SetupTestDriver(browser, url);
		this.driver = setupTestDriver.getDriver();
		
		locale = new Locale(lang);
		messages = ResourceBundle.getBundle("messages", locale);
	}

	@Test
	public void testMenuItems() {
		/* Verify the menus in the application */
		
		
		homePage = new HomePage(driver);

		Assert.assertTrue(homePage.isMenuDisplayed(messages.getString("menu1Books")));
		Assert.assertTrue(homePage.isMenuDisplayed(messages.getString("menu2Computers")));
		Assert.assertTrue(homePage.isMenuDisplayed(messages.getString("menu3Electronics")));

	}

	@Test(dependsOnMethods = "testMenuItems")
	public void testMenuSelection() {

		/* Select the menu and verify */
		
		// homePage.selectMenu("Books");
		homePage.selectMenu("Computers");
		homePage.selectMenu("Electronics");
	}

	@AfterMethod
	public void tearDown() {
		// Close the browser after each test
		driver.quit();
	}
}