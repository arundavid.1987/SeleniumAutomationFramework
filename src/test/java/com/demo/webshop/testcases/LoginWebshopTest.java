package com.demo.webshop.testcases;

import java.util.Locale;
import java.util.ResourceBundle;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import com.demo.webshop.pages.LoginPage;
import com.utils.setupdriver.SetupTestDriver;

public class LoginWebshopTest {	
	private WebDriver driver;
	private LoginPage loginPage;
	Locale locale;
	ResourceBundle messages;

	@BeforeMethod(alwaysRun = true)
	@Parameters({ "browser", "url", "lang" })
	public void setUp(String browser, String url, String lang) throws Exception {

		SetupTestDriver setupTestDriver = new SetupTestDriver(browser, url);
		this.driver = setupTestDriver.getDriver();

		locale = new Locale(lang);
		messages = ResourceBundle.getBundle("messages", locale);
	}

	@Test
	public void registerExistingUser() throws InterruptedException {
		String existingUserError = messages.getString("existingUserError");

		/* Register an existing user */
		loginPage = new LoginPage(this.driver);
		loginPage.NavigateTo("Register");
		loginPage.registerUser("sam", "dan", "a1z@a.com", "tester1234");

		loginPage.verifyExistingUserError(existingUserError);
	}

	@Test
	public void registerNewUser() throws InterruptedException {

		/* Register New User */
		loginPage = new LoginPage(driver);
		loginPage.NavigateTo("Register");
		loginPage.registerUser("sam", "dan", "a1z@a.com", "tester1234");
		loginPage.verifyUserIsLoggedIn();
	}

	@Test
	public void LoginExistingUser() throws InterruptedException {

		/* Login with Existing user and verify */
		loginPage = new LoginPage(driver);
		loginPage.NavigateTo("Login");
		loginPage.Login("sam@a.com", "test123");
		loginPage.verifyUserIsLoggedIn();
	}

	@AfterMethod
	public void tearDown() {
		this.driver.quit();
	}
}