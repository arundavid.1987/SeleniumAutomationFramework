package com.demo.webshop.testcases;

import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizationExample {

    public static void main(String[] args) {
        // Set the desired language (e.g., "en" for English, "fr" for French)
        String language = "fr"; // Change this value to test different languages

        // Set the locale based on the selected language
        Locale locale = new Locale(language);

        // Load the appropriate resource bundle based on the locale
        ResourceBundle messages = ResourceBundle.getBundle("messages", locale);

        // Get the localized values using the keys from the property file
        String greeting = messages.getString("greeting");
        String loginButton = messages.getString("loginButton");

        // Print the localized values
        System.out.println("Greeting: " + greeting);
        System.out.println("Login Button: " + loginButton);
    }
}
