package com.demo.webshop.pages;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class LoginPage {
    public WebDriver driver;

    @FindBy(xpath = "//div[@class='header-links']/ul/li[1]/a")
    public WebElement registerLink;

    @FindBy(xpath = "//div[@class='header-links']//li[2]/a")
    public WebElement loginLink;

    @FindBy(xpath = "//div[@class='page-title']/h1")
    public WebElement pageTitle;
    
    @FindBy(css = "#gender-male")
    public WebElement genderMale;
    
    @FindBy(css = "#gender-female")
    public WebElement genderFemale;
    
    @FindBy(css = "#FirstName")
    public WebElement firstNameTextBox;
    
    @FindBy(css = "#LastName")
    public WebElement lastNameTextBox;
    
    @FindBy(css = "#Email")
    public WebElement emailTextBox;
    
    @FindBy(css = "#Password")
    public WebElement passwordTextBox;
    
    @FindBy(css = "#ConfirmPassword")
    public WebElement confirmPassTextBox;
    
    @FindBy(css = "#register-button")
    public WebElement registerButton;
    
    @FindBy(css = ".validation-summary-errors")
    public WebElement validationError;
  
    @FindBy(xpath = "//li/a[@class='ico-logout']")
    public WebElement logoutLink;
  
    @FindBy(xpath = "//input[@value='Log in']")
    public WebElement loginButton;

    // Add more menu elements as needed

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void NavigateTo(String headerLink) throws InterruptedException {
    	 switch (headerLink.toLowerCase()) {
         case "register":
        	 registerLink.click();
        	 Assert.assertEquals(pageTitle.getText(),"Register");
        	 break;
         case "login":
        	 loginLink.click();
        	 break;
         case "shopping cart":
        	 loginLink.click();
        	 break;
         default:
             throw new IllegalArgumentException("Header link not available: " + headerLink);
     }
    }
    
    public void registerUser(String Fname, String Lname, String Email, String Pword) {	
    	genderFemale.click();
    	firstNameTextBox.sendKeys(Fname);
    	lastNameTextBox.sendKeys(Lname);
    	emailTextBox.sendKeys(Email);
    	passwordTextBox.sendKeys(Pword);
    	confirmPassTextBox.sendKeys(Pword);
    	registerButton.click();
    }
    
    public void Login(String Email, String Pword) {	

    	emailTextBox.sendKeys(Email);
    	passwordTextBox.sendKeys(Pword);
    	loginButton.click();
    }
    
    public void verifyExistingUserError(String expectedMsg) {
    	
    	Assert.assertTrue(validationError.isDisplayed(),"Validation Error missing");
    	
    	String actualError = validationError.getText();
    	Assert.assertEquals(actualError.toLowerCase(), expectedMsg.toLowerCase());
    }
    
    public void verifyUserIsLoggedIn( ) {
    	Assert.assertTrue(logoutLink.isDisplayed(),"Logout Link not displayed");
    }
}