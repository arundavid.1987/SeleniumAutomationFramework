package com.demo.webshop.pages;
import org.apache.poi.hssf.record.PageBreakRecord.Break;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;

    @FindBy(xpath = "//ul[@class='top-menu']/li[1]/a")
    private WebElement menu1;

    @FindBy(xpath = "//ul[@class='top-menu']/li[2]/a")
    private WebElement menu2;

    @FindBy(xpath = "//ul[@class='top-menu']/li[3]/a")
    private WebElement menu3;

    // Add more menu elements as needed

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isMenuDisplayed(String menuName) {
        switch (menuName) {
            case "Books":
                return menu1.isDisplayed();
            case "Computers":
                return menu2.isDisplayed();
            case "Electronics":
                return menu3.isDisplayed();
            // Add more cases for other menu elements
            default:
                throw new IllegalArgumentException("Invalid menu name: " + menuName);
        }
    }
    
    public void selectMenu(String menuName) {
        switch (menuName) {
            case "Books":
                 menu1.click();
                 break;
            case "Computers":
            	menu2.click();
            	break;
            case "Electronics":
            	menu3.click();
            	break;
            default:
                throw new IllegalArgumentException("menu not clickable: " + menuName);
        }
    }
}
