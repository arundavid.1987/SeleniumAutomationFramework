package com.demo.autoInsurance.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage {
    private WebDriver driver;

    @FindBy(xpath = "//div[@class='main-navigation']//a[@id='nav_automobile']")
    private WebElement menu1;

    @FindBy(xpath = "//div[@class='main-navigation']//a[@id='nav_truck']")
    private WebElement menu2;

    @FindBy(xpath = "//div[@class='main-navigation']//a[@id='nav_motorcycle']")
    private WebElement menu3;

    // Add more menu elements as needed

    public HomePage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public boolean isMenuDisplayed(String menuName) {
        switch (menuName) {
            case "Automobile":
                return menu1.isDisplayed();
            case "Truck":
                return menu2.isDisplayed();
            case "Motorcycle":
                return menu3.isDisplayed();
            // Add more cases for other menu elements
            default:
                throw new IllegalArgumentException("Invalid menu name: " + menuName);
        }
    }
    
    public void selectMenu(String menuName) {
        switch (menuName) {
            case "Automobile":
                 menu1.click();
            case "Truck":
            	menu2.click();
            case "Motorcycle":
            	menu3.click();
            default:
                throw new IllegalArgumentException("menu not clickable: " + menuName);
        }
    }
}
